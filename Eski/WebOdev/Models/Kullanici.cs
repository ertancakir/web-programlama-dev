﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebOdev.Models
{
    public class Kullanici
    {
        public int KullaniciID { get; set; }
        public string Adi { get; set; }
        public string Sifre { get; set; }
    }
}