﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace WebOdev.Models
{
    public class oyunContext : DbContext
    {
        public DbSet<Oyun> Oyun { get; set; }
        public DbSet<Kullanici> Kullanici { get; set; }
        public DbSet<Yorum> Yorum { get; set; }
    }
}