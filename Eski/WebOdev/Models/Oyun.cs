﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebOdev.Models
{
    public class Oyun
    {
        [Key]
        public int OyunID { get; set; }
        public string Adi { get; set; }
        public int Fiyat { get; set; }
        public string SistemGereksinimleri { get; set; }

        public virtual ICollection<Yorum> Yorumlar { get; set; }
    }
}