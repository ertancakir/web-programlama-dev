﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebOdev.Models
{
    public class Yorum
    {
        public int YorumID { get; set; }
        public string yorum { get; set; }
        public DateTime Tarih { get; set; }

        public virtual Kullanici Yazar { get; set; }
    }
}