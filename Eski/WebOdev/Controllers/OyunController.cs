﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebOdev.Models;

namespace WebOdev.Controllers
{
    public class OyunController : Controller
    {
        private oyunContext db = new oyunContext();

        // GET: Oyun
        public ActionResult Index()
        {
            return View(db.Oyun.ToList());
        }

        // GET: Oyun/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyun.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // GET: Oyun/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Oyun/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "OyunID,Adi,Fiyat,SistemGereksinimleri")] Oyun oyun)
        {
            if (ModelState.IsValid)
            {
                db.Oyun.Add(oyun);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(oyun);
        }

        // GET: Oyun/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyun.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // POST: Oyun/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "OyunID,Adi,Fiyat,SistemGereksinimleri")] Oyun oyun)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oyun).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(oyun);
        }

        // GET: Oyun/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyun.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // POST: Oyun/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oyun oyun = db.Oyun.Find(id);
            db.Oyun.Remove(oyun);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
