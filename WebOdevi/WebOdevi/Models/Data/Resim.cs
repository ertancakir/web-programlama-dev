﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebOdevi.Models.Data
{
    public class Resim
    {
        [Key]
        public int ResimID { get; set; }

        public virtual Oyun oyun { get; set; }

        [Display(Name = "Resim")]
        public byte[] oyunFoto { get; set; }
    }
}