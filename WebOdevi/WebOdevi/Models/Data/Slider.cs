﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebOdevi.Models.Data
{
    public class Slider
    {
        [Key]
        public int SliderID { get; set; }
        [Display(Name = "Resim")]
        public byte[] SliderFoto { get; set; }
        [Display(Name = "Yazı")]
        public string SliderText { get; set; }
        [Display(Name = "Başlangıç Tarih")]
        public Nullable<System.DateTime> BaslangicTarih { get; set; }
        [Display(Name = "Bitiş Tarih")]
        public Nullable<System.DateTime> BitisTarih { get; set; }
    }
}