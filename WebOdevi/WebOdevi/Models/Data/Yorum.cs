﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebOdevi.Models.Data
{
    public class Yorum
    {
        [Key]
        public int YorumID { get; set; }
        [Display(Name = "Mesaj")]
        public string yorum { get; set; }
        [Display(Name = "Kullanıcı Adı")]
        public string k_Adi { get; set; }
        [Display(Name = "Yorum Onayı")]
        public bool onay { get; set; }
        [Display(Name = "Tarih")]
        public DateTime yorumTarih { get; set; }
        public virtual Oyun oyun { get; set; }
        //public virtual Kullanici User { get; set; }
    }
}