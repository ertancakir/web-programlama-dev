﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebOdevi.Models.Data
{
    public class Oyun
    {
        [Key]
        public int OyunID { get; set; }
        [Display(Name = "Resim")]
        public byte[] oyunFoto { get; set; }
        [Display(Name = "Oyun Adı")]
        public string Adi { get; set; }
        [Display(Name = "Ücret")]
        public int Fiyat { get; set; }
        [Display(Name = "Oyun Türü")]
        public OyunTuru Tur { get; set; }
        [Display(Name = "Oyun Hakkında Açıklamalar")]
        public string aciklama { get; set; }
        public int tikSayi { get; set; }
        public bool haftaninOyunu { get; set; }
        public virtual ICollection<Yorum> Yorumlar { get; set; }
    }

    public enum OyunTuru
    {
        MMORPG, FPS, RPG, MOBA, MMO
    }
}