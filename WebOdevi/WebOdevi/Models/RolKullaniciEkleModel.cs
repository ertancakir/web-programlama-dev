﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebOdevi.Models
{
    public class RolKullaniciEkleModel
    {
        [Display(Name = "Kullanını Adı")]
        public string KullaniciAdi { get; set; }
        [Display(Name = "Rol Adı")]
        public string RolAdi { get; set; }
    }
}