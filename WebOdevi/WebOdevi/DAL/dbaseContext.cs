﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using WebOdevi.Models.Data;

namespace WebOdevi.DAL
{
    public class dbaseContext : DbContext
    {
        public DbSet<Oyun> Oyun { get; set; }
        public DbSet<Yorum> Yorum { get; set; }
        public DbSet<Resim> Resim { get; set; }
        public DbSet<Slider> Slider { get; set; }
    }
}