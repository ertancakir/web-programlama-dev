﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WebOdevi.Startup))]
namespace WebOdevi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
