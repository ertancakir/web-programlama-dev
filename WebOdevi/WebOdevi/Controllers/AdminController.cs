﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebOdevi.DAL;
using WebOdevi.Models;
using WebOdevi.Models.Data;

namespace WebOdevi.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private dbaseContext db = new dbaseContext();

        public void yorumBadge()
        {
            ViewBag.yorumSayi = db.Yorum.Where(x => x.onay == false).Count();
        }
        public ActionResult Index()
        {
            yorumBadge();
            var model = db.Oyun.ToList();
            return View(model);
        }

        public ActionResult HaftaninOyunu(int id)
        {
            Oyun eski = db.Oyun.Where(x => x.haftaninOyunu == true).FirstOrDefault();
            if(eski != null)
            {
                eski.haftaninOyunu = false;
            }
            Oyun yeni = db.Oyun.Find(id);
            yeni.haftaninOyunu = true;
            db.SaveChanges();
            return RedirectToAction("Index"); 
        }

        public ActionResult Resim()
        {
            yorumBadge();
            return View(db.Resim.ToList());
        }
        public ActionResult ResimEkle()
        {
            yorumBadge();
            var query = db.Oyun.Select(o => new { o.OyunID, o.Adi });
            ViewBag.CategoryId = new SelectList(query.AsEnumerable(), "OyunID", "Adi", 3);
            return View();
        }
        [HttpPost]
        public ActionResult ResimEkle(Resim r, HttpPostedFileBase file)
        {
            using (dbaseContext context = new dbaseContext())
            {
                Resim resim = new Resim();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream memoryStream = file.InputStream as MemoryStream;
                    if (memoryStream == null)
                    {
                        memoryStream = new MemoryStream();
                        file.InputStream.CopyTo(memoryStream);
                    }
                    resim.oyunFoto = memoryStream.ToArray();
                }
                resim.oyun = context.Oyun.Find(r.oyun.OyunID);
                context.Resim.Add(resim);
                context.SaveChanges();
                return RedirectToAction("Resim", "Admin");
            }
        }
        // GET: Admin/Details/5
        public ActionResult OyunDetay(int? id)
        {
            yorumBadge();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyun.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // GET: Admin/Create
        public ActionResult OyunEkle()
        {
            yorumBadge();
            return View();
        }
        [HttpPost]
        public ActionResult OyunEkle(Oyun o, HttpPostedFileBase file)
        {
            try
            {
                using (dbaseContext context = new dbaseContext())
                {
                    Oyun oyun = new Oyun();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        oyun.oyunFoto = memoryStream.ToArray();
                    }
                    oyun.Adi = o.Adi;
                    oyun.Fiyat = o.Fiyat;
                    oyun.aciklama = o.aciklama;
                    oyun.Tur = o.Tur;
                    oyun.tikSayi = 0;
                    context.Oyun.Add(oyun);
                    context.SaveChanges();
                    return RedirectToAction("Index", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu");
            }
        }

        public ActionResult OyunDuzenle(int? id)
        {
            yorumBadge();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyun.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }
        

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult OyunDuzenle(Oyun o, HttpPostedFileBase file)
        {
            try
            {
                using (dbaseContext context = new dbaseContext())
                {
                    var oyunDuzenle = context.Oyun.Where(x => x.OyunID == o.OyunID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        oyunDuzenle.oyunFoto = memoryStream.ToArray();
                    }
                    oyunDuzenle.Adi = o.Adi;
                    oyunDuzenle.aciklama = o.aciklama;
                    oyunDuzenle.Fiyat = o.Fiyat;
                    oyunDuzenle.haftaninOyunu = o.haftaninOyunu;
                    oyunDuzenle.tikSayi = o.tikSayi;
                    oyunDuzenle.Tur = o.Tur;
                    oyunDuzenle.Yorumlar = o.Yorumlar;
                    context.SaveChanges();
                    return RedirectToAction("Index", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }
        }

        // GET: Admin/Delete/5
        public ActionResult OyunSil(int? id)
        {
            yorumBadge();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oyun oyun = db.Oyun.Find(id);
            if (oyun == null)
            {
                return HttpNotFound();
            }
            return View(oyun);
        }

        // POST: Admin/Delete/5
        [HttpPost, ActionName("OyunSil")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oyun oyun = db.Oyun.Find(id);
            db.Oyun.Remove(oyun);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        ApplicationDbContext context = new ApplicationDbContext();
        public ActionResult Rol()
        {
            yorumBadge();
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            return View(roleManager.Roles.ToList());
        }

        public ActionResult RolEkle()
        {
            yorumBadge();
            return View();
        }

        [HttpPost]
        public ActionResult RolEkle(RolModel rol)
        {
            yorumBadge();
            var roleStore = new RoleStore<IdentityRole>(context);
            var roleManager = new RoleManager<IdentityRole>(roleStore);

            if (roleManager.RoleExists(rol.RolAdi) == false)
            {
                roleManager.Create(new IdentityRole(rol.RolAdi));
            }
            return RedirectToAction("Rol");
        }

        public ActionResult RolKullaniciEkle()
        {
            yorumBadge();
            return View();
        }

        [HttpPost]
        public ActionResult RolKullaniciEkle(RolKullaniciEkleModel model)
        {
            var userStore = new UserStore<ApplicationUser>(context);
            var userManager = new UserManager<ApplicationUser>(userStore);

            var kullanici = userManager.FindByName(model.KullaniciAdi);

            userManager.AddToRole(kullanici.Id, model.RolAdi);

            return RedirectToAction("Rol");
        }


        public ActionResult Slider()
        {
            yorumBadge();
            using (dbaseContext db = new dbaseContext())
            {
                return View(db.Slider.ToList());
            }
        }

        public ActionResult SlideEkle()
        {
            yorumBadge();
            return View();
        }

        public ActionResult SlideDuzenle(int id)
        {
            yorumBadge();
            using (dbaseContext context = new dbaseContext())
            {
                var _slideDuzenle = context.Slider.Where(x => x.SliderID == id).FirstOrDefault();
                return View(_slideDuzenle);
            }
        }

        [HttpPost]
        public ActionResult SlideEkle(Slider s, HttpPostedFileBase file)
        {
            try
            {
                using (dbaseContext context = new dbaseContext())
                {
                    Slider _slide = new Slider();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _slide.SliderFoto = memoryStream.ToArray();
                    }
                    _slide.SliderText = s.SliderText;
                    _slide.BaslangicTarih = s.BaslangicTarih;
                    _slide.BitisTarih = s.BitisTarih;
                    context.Slider.Add(_slide);
                    context.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Eklerken hata oluştu" + ex);
            }
        }
        [HttpPost]
        public ActionResult SlideDuzenle(Slider slide, HttpPostedFileBase file)
        {
            try
            {
                using (dbaseContext context = new dbaseContext())
                {
                    var _slideDuzenle = context.Slider.Where(x => x.SliderID == slide.SliderID).FirstOrDefault();
                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream memoryStream = file.InputStream as MemoryStream;
                        if (memoryStream == null)
                        {
                            memoryStream = new MemoryStream();
                            file.InputStream.CopyTo(memoryStream);
                        }
                        _slideDuzenle.SliderFoto = memoryStream.ToArray();
                    }
                    _slideDuzenle.SliderText = slide.SliderText;
                    _slideDuzenle.BaslangicTarih = slide.BaslangicTarih;
                    _slideDuzenle.BitisTarih = slide.BitisTarih;
                    context.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Güncellerken hata oluştu " + ex.Message);
            }

        }
        public ActionResult SlideSil(int id)
        {
            yorumBadge();
            try
            {
                using (dbaseContext context = new dbaseContext())
                {
                    context.Slider.Remove(context.Slider.First(d => d.SliderID == id));
                    context.SaveChanges();
                    return RedirectToAction("Slider", "Admin");
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Silerken hata oluştu", ex.InnerException);
            }
        }

        public ActionResult Yorum()
        {
            yorumBadge();
            var model = db.Yorum.Where(x => x.onay == false).OrderByDescending(x => x.YorumID).ToList();
            return View(model);
        }

        public ActionResult YorumOnayla(int id)
        {
            Yorum yorum = db.Yorum.Find(id);
            yorum.onay = true;
            db.SaveChanges();
            return RedirectToAction("Yorum");
        }
    }

    public class Admin
    {
        public Oyun _oyun { get; set; }
        public List<Oyun> oyun { get; set; }
        public List<Resim> resim { get; set; }
    }
}
