﻿using System;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebOdevi.Models.Data;
using WebOdevi.Models;
using WebOdevi.DAL;
using System.Net;
using Microsoft.AspNet.Identity.EntityFramework;

namespace WebOdevi.Controllers
{
    public class HomeController : Controller
    {
        private bool IsAdmin()
        {
            bool result;
            result = false;
            if (User.Identity.GetUserId() != null)
            {
                ApplicationDbContext context = new ApplicationDbContext();
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                var kullanici = userManager.FindByName(User.Identity.GetUserName());
                if (userManager.GetRoles(User.Identity.GetUserId()).ToList().FirstOrDefault() == "Admin")
                {
                    result = true;
                }
            }
            return result;
        }
        public ActionResult Deneme()
        {
            return View();
        }
        public ActionResult Index(string aranan)
        {
            ViewBag.IsAdmin = IsAdmin();
            ViewBag.isIndex = true;
            if(aranan != null)
            {
                using (dbaseContext db = new dbaseContext())
                {
                    Anasayfa anasayfa = new Anasayfa();
                    anasayfa.slider = db.Slider.Where(x => (x.BaslangicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList();
                    anasayfa.oyun = db.Oyun.Where(x => x.Adi.ToUpper().Contains(aranan.ToUpper().ToString())).ToList();  //Arama için gerekli LİNQ komutu
                    anasayfa.yorum = db.Yorum.OrderByDescending(x => x.YorumID).ToList();
                    anasayfa.yorum = anasayfa.yorum.Take(3).ToList();
                    anasayfa.populerOyun = db.Oyun.OrderByDescending(x => x.tikSayi).ToList();
                    anasayfa._oyun = db.Oyun.Where(x => x.haftaninOyunu == true).FirstOrDefault();
                    return View(anasayfa);
                }
            }
            using (dbaseContext db = new dbaseContext())
            {
                Anasayfa anasayfa = new Anasayfa();
                anasayfa.slider = db.Slider.Where(x => (x.BaslangicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList();
                anasayfa.oyun = db.Oyun.OrderByDescending(x=>x.OyunID).ToList();
                anasayfa.populerOyun = db.Oyun.OrderByDescending(x => x.tikSayi).ToList();
                anasayfa.yorum = db.Yorum.OrderByDescending(x => x.YorumID).ToList();
                return View(anasayfa);
            }
        }

        public ActionResult About()
        {
            ViewBag.IsAdmin = IsAdmin();
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.IsAdmin = IsAdmin();
            ViewBag.Message = "Your contact page.";
            return View();
        }
        public ActionResult Oyun(int? id)
        {
            if(id == null)
            {
                return RedirectToAction("Index");
            }
            ViewBag.IsAdmin = IsAdmin();
            using (dbaseContext db = new dbaseContext())
            {
                Oyun oyun = db.Oyun.Find(id);
                oyun.tikSayi += 1;
                db.SaveChanges();
                Anasayfa anasayfa = new Anasayfa();
                anasayfa._oyun = db.Oyun.Where(x => x.OyunID == id).FirstOrDefault();
                anasayfa.resim = db.Resim.Where(x => x.oyun.OyunID == id).OrderByDescending(x=>x.ResimID).Take(3).ToList();
                anasayfa.yorum = db.Yorum.Where(x => x.oyun.OyunID == id && x.onay == true).OrderByDescending(x => x.YorumID).Take(5).ToList();
                anasayfa.oyun = db.Oyun.Where(x => x.Tur == oyun.Tur).Take(3).ToList();
                return View(anasayfa);
            }
        }
        [HttpPost]
        public ActionResult Oyun(Anasayfa a,int id)
        {
            using (dbaseContext db = new dbaseContext())
            {
                Yorum y = new Yorum();
                y.oyun = db.Oyun.Find(id);
                y.yorum = a.YorumGovde;
                y.k_Adi = User.Identity.GetUserName();
                y.onay = false;
                y.yorumTarih = DateTime.Now;
                db.Yorum.Add(y);
                db.SaveChanges();
                return RedirectToAction("Oyun/"+id, "Home");
            }
        }

        [HttpGet]
        public ActionResult Oyunlar()
        {
            ViewBag.IsAdmin = IsAdmin();
            using (dbaseContext db = new dbaseContext())
            {
                Anasayfa anasayfa = new Anasayfa();
                anasayfa.slider = db.Slider.Where(x => (x.BaslangicTarih <= DateTime.Now && x.BitisTarih > DateTime.Now)).ToList();
                anasayfa.oyun = db.Oyun.OrderBy(x => x.OyunID).ToList();
                return View(anasayfa);
            }
        }
    }
    public class Anasayfa
    {
        public int OyunID { get; set; }
        public string YorumGovde { get; set; }
        public Oyun _oyun { get; set; }
        public Yorum _yorum { get; set; }
        public List<Oyun> oyun { get; set; }
        public List<Oyun> populerOyun { get; set; }
        public List<Yorum> yorum { get; set; }
        public List<Resim> resim { get; set; }
        public List<Slider> slider { get; set; }
    }
}