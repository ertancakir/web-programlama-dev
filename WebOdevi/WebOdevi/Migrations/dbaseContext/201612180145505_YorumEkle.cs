namespace WebOdevi.Migrations.dbaseContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class YorumEkle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Yorums", "onay", c => c.Boolean(nullable: false));
            AddColumn("dbo.Yorums", "yorumTarih", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Yorums", "yorumTarih");
            DropColumn("dbo.Yorums", "onay");
        }
    }
}
