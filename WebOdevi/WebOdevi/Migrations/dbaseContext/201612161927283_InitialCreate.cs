namespace WebOdevi.Migrations.dbaseContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Oyuns",
                c => new
                    {
                        OyunID = c.Int(nullable: false, identity: true),
                        oyunFoto = c.Binary(),
                        Adi = c.String(),
                        Fiyat = c.Int(nullable: false),
                        Tur = c.Int(nullable: false),
                        aciklama = c.String(),
                    })
                .PrimaryKey(t => t.OyunID);
            
            CreateTable(
                "dbo.Yorums",
                c => new
                    {
                        YorumID = c.Int(nullable: false, identity: true),
                        yorum = c.String(),
                        k_Adi = c.String(),
                        oyun_OyunID = c.Int(),
                    })
                .PrimaryKey(t => t.YorumID)
                .ForeignKey("dbo.Oyuns", t => t.oyun_OyunID)
                .Index(t => t.oyun_OyunID);
            
            CreateTable(
                "dbo.Resims",
                c => new
                    {
                        ResimID = c.Int(nullable: false, identity: true),
                        oyunFoto = c.Binary(),
                        oyun_OyunID = c.Int(),
                    })
                .PrimaryKey(t => t.ResimID)
                .ForeignKey("dbo.Oyuns", t => t.oyun_OyunID)
                .Index(t => t.oyun_OyunID);
            
            CreateTable(
                "dbo.Sliders",
                c => new
                    {
                        SliderID = c.Int(nullable: false, identity: true),
                        SliderFoto = c.Binary(),
                        SliderText = c.String(),
                        BaslangicTarih = c.DateTime(),
                        BitisTarih = c.DateTime(),
                    })
                .PrimaryKey(t => t.SliderID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Resims", "oyun_OyunID", "dbo.Oyuns");
            DropForeignKey("dbo.Yorums", "oyun_OyunID", "dbo.Oyuns");
            DropIndex("dbo.Resims", new[] { "oyun_OyunID" });
            DropIndex("dbo.Yorums", new[] { "oyun_OyunID" });
            DropTable("dbo.Sliders");
            DropTable("dbo.Resims");
            DropTable("dbo.Yorums");
            DropTable("dbo.Oyuns");
        }
    }
}
