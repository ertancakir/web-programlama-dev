namespace WebOdevi.Migrations.dbaseContext
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class OyunModelDuzenleme : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Oyuns", "tikSayi", c => c.Int(nullable: false));
            AddColumn("dbo.Oyuns", "haftaninOyunu", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Oyuns", "haftaninOyunu");
            DropColumn("dbo.Oyuns", "tikSayi");
        }
    }
}
